package com.ps;

public class Game {
    
    public int rollOne(int rollResult){
        
        var score = rollResult;
        
        return score;
    }

    public int singleFrameScore(int[] frameScore){
        var score = 0;

        for (int i=0; i<frameScore.length; i++) {
            score += frameScore[i];
        } 

        return score;
    }

    int frame = 1;
    int roll = 1;
    public int fullGameScore(int[] gameScore){
        var score = 0;

        for (int i=0; i<gameScore.length; i++) {
           
            if (frame == 10){
                score = tenthFrame(gameScore, score, roll, i);
                roll += 1;
            }else{ 

                if (roll == 1){
                    score = strikeCheck(gameScore, score, i);
                } else if (roll == 2){
                    score = spareCheck(gameScore, score, i);
                }
            
                score += gameScore[i];
            } 
        } 

        return score;
    }

    public int tenthFrame(int[] gameScore, int score, int roll, int i){
        int pinsDown = gameScore[i];
        int pinsDownAfterTwoRolls = gameScore[i] + gameScore[i-1];
        
        if (roll == 1){
            if (pinsDown == 10){
                score += gameScore[i+1] + gameScore[i+2] + gameScore[i];
            } else {
                score += gameScore[i];
            }
            
        } else if (roll == 2){
            if (pinsDownAfterTwoRolls == 10){
                score += gameScore[i+1] + gameScore[i];
            } else if (pinsDownAfterTwoRolls < 10) {
                score += gameScore[i];
            }
        }

        return score;
    }

    public int strikeCheck (int[] gameScore, int score, int i){
        int pinsDown = gameScore[i];
            if(pinsDown == 10){
                score += gameScore[i+1];
                score += gameScore[i+2];
                frame += 1;
            } else {
                roll = 2;
            }

        return score;
    }

    public int spareCheck (int[] gameScore, int score, int i){
        int pinsDown = gameScore[i] + gameScore[i-1];
            
        if (pinsDown == 10){
            score += gameScore[i+1];
        }

        frame += 1;
        roll = 1;


        return score;
    }
}

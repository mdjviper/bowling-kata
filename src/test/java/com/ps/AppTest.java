package com.ps;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void canReturnScore(){
        
        var rollResult = 2;

        var game =  new Game();
        var score = game.rollOne(rollResult);

        assertEquals(2, score);
    }
    
    @Test
    public void canScoreOneFrameTest(){
        
        var rollOne = 2;
        var rollTwo = 1;
        int frameScore[] = {rollOne, rollTwo};

        var game = new Game();
        var score = game.singleFrameScore(frameScore);

        assertEquals(3, score);
    }

    @Test
    public void strikeLogicTest(){

        var rollOne = 10;
        int frameScore[] = {rollOne};
        var game = new Game();
        var score = game.singleFrameScore(frameScore);

        assertEquals(10, score);
    }

    @Test
    public void fullGameTest(){

        int gameScores[] = {2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(40, score);
    }

    @Test
    public void oneStrikeTest(){

        int gameScores[] = {10, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(50, score);
    }

    @Test
    public void threeStrikeTest(){

        int gameScores[] = {10, 2,2, 10, 2,2, 2,2, 2,2, 10, 2,2, 2,2, 2,2};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(70, score);
    }

    @Test
    public void threeSpareTest(){

        int gameScores[] = {5,5, 2,2, 5,5, 2,2, 2,2, 2,2, 5,5, 2,2, 2,2, 2,2};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(64, score);
    }

    @Test
    public void allStrikesTest(){

        int gameScores[] = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(300, score);
    }

    @Test
    public void lastFrameSpareTest(){

        int gameScores[] = {2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,2, 2,8,5};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(51, score);
    }

    @Test
    public void randomGameTest(){

        int gameScores[] = {2,6, 4,6, 10, 5,2, 8,1, 9,1, 2,2, 10, 2,2, 2,8,5};
        var game = new Game();
        var score = game.fullGameScore(gameScores);

        assertEquals(110, score);
    }
}
